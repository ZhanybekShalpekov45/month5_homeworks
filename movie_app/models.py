from django.db import models
from django.db.models import Avg
import uuid
STARS = (
    (1, '⭐'),
    (2, '⭐⭐'),
    (3, '⭐⭐⭐'),
    (4, '⭐⭐⭐⭐'),
    (5, '⭐⭐⭐⭐⭐'),
)


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True


class MovieApp(BaseModel):
    title = models.CharField(max_length=100)
    description = models.TextField()
    duration = models.CharField(max_length=100)
    director = models.ForeignKey(to='DirectorsList', on_delete=models.CASCADE, related_name='directors')
    reviews = models.ForeignKey(to='ReviewsList', on_delete=models.CASCADE, related_name='reviews')

    class Meta:
        verbose_name = 'film'
        verbose_name_plural = 'films'

    def __str__(self):
        return f"Movie {self.title}"


class ReviewsList(BaseModel):
    text = models.CharField(max_length=500)
    rating = models.IntegerField(null=False, blank=False, choices=STARS, default=5)
    movie = models.ForeignKey(MovieApp, on_delete=models.CASCADE, related_name='movie_reviews')

    def movie_str(self):
        if self.movie:
            return self.movie.name
        return None

    def __str__(self):
        return f'Review:{self.text:50}'

    class Meta:
        verbose_name = 'review'
        verbose_name_plural = 'reviews'


class Comment(BaseModel):
    text = models.TextField()
    movie = models.ForeignKey(MovieApp, on_delete=models.CASCADE, related_name='comments')

    class Meta:
        verbose_name = 'comment'
        verbose_name_plural = 'comments'

    def __str__(self):
        return f'comment : {self.text}'


class DirectorsList(BaseModel):
    director = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Director'
        verbose_name_plural = 'Directors'

    def __str__(self):
        return f'Directors: {self.director}'
