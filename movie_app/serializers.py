from django.db.models import Avg
from rest_framework import serializers
from movie_app.models import MovieApp, Comment, DirectorsList, ReviewsList
from django.shortcuts import render


class DirectorsListSerializer(serializers.ModelSerializer):
    movies_count = serializers.SerializerMethodField()

    class Meta:
        model = DirectorsList
        fields = ('id', 'name', 'movies_count')

    def get_movies_count(self):
        return self.movies_count


class DirectorDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = DirectorsList
        fields = "__all__"


class CommentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'text')


class CommentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class MovieDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovieApp
        fields = "__all__"


class ReviewsListSerializer(serializers.ModelSerializer):
    average_rating = serializers.SerializerMethodField()

    class Meta:
        model = ReviewsList
        fields = ('id', 'text', 'stars')

    def get_average_rating(self, rating):
        average_rating = ReviewsList.objects.filter(movie=rating.movie).aggregate(average_rating=Avg('rating'))
        return round(average_rating['average_rating'], 1) if average_rating['average_rating'] else None


class ReviewsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewsList
        fields = "__all__"


class MovieListSerializer(serializers.ModelSerializer):
    reviews = ReviewsListSerializer(many=True)

    class Meta:
        model = MovieApp
        fields = ('id', 'title', 'description', 'duration', 'text', 'movie')


class MovieValidateSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    duration = serializers.IntegerField(required=True)
    director = serializers.PrimaryKeyRelatedField(required=True, queryset=DirectorsList.objects.all())

    def validate_director(self, director_id):
        try:
            DirectorsList.objects.get(id=director_id)
        except DirectorsList.DoesNotExist as e:
            raise serializers.ValidationError(str(e))

        return director_id

    def create(self, validated_data):
        title = validated_data.get('title')
        description = validated_data.get('description')
        duration = validated_data.get('duration')
        director = validated_data.get('director')

        movie = MovieApp.objects.create(
            title=title, description=description, duration=duration
        )

        movie.director.set(director)
        return movie

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.content)
        instance.duration = validated_data.get('duration', instance.category_id)
        instance.director.set(validated_data.get('director', instance.director.all()))
        instance.save()

        return instance


class ReviewsValidateSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)
    rating = serializers.CharField(required=True)
    movie = serializers.PrimaryKeyRelatedField(required=True, queryset=MovieApp.objects.all())

    def validate_rating(self, value:list):
        try:
            ReviewsList.objects.get(id=value)
        except ReviewsList.objects.DoesNotExist as e:
            raise serializers.ValidationError(str(e))
        return value

    def validate_movie(self, value:list):
        try:
            ReviewsList.objects.get(id=value)
        except ReviewsList.objects.DoesNotExist as e:
            raise serializers.ValidationError(str(e))
        return value


class CommentsValidateSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)
    movie = serializers.PrimaryKeyRelatedField(required=True, queryset=MovieApp.objects.all())

    def validate_movie(self, value: list):
        try:
            Comment.objects.get(id=value)
        except Comment.objects.DoesNotExist as e:
            raise serializers.ValidationError(str(e))
        return value

    def create(self, validated_data):
        text = validated_data.get('text')
        movie = validated_data.get('movie')

        comment = Comment.objects.create(
            text=text, movie=movie
        )

        return comment

    def update(self, instance, validated_data):
        instance.text = validated_data.get('text', instance.text)
        instance.movie = validated_data.get('movie', instance.movie)
        instance.save()

        return instance