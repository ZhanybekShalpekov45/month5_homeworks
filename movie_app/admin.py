from django.contrib import admin
from django.http import HttpRequest

from movie_app.models import MovieApp, Comment, DirectorsList, ReviewsList


@admin.register(ReviewsList)
class ReviewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'rating', 'movie')
    list_display_links = ('id', 'text')
    search_fields = ('text',)
    list_filter = ('rating',)
    fields = ('text', 'rating', 'movie')

    def has_add_permission(self, request: HttpRequest) -> bool:
        return True

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False


@admin.register(MovieApp)
class MovieAppAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'duration', 'director')
    list_display_links = ('id', 'title')
    search_fields = ('title',)
    list_filter = ('director',)
    fields = ('title', 'slug', 'description', 'duration', 'director')
    prepopulated_fields = {'slug': ('title',), }

    def has_add_permission(self, request: HttpRequest) -> bool:
        return True

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'movie')
    list_display_links = ('id', 'text')
    search_fields = ('text',)
    list_filter = ('movie',)
    fields = ('text', 'movie')

    def has_add_permission(self, request: HttpRequest) -> bool:
        return True

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False


@admin.register(DirectorsList)
class DirectorsListAdmin(admin.ModelAdmin):
    list_display = ('id', 'director')
    list_display_links = ('id', 'director')
    search_fields = ('director',)
    fields = ('director',)

    def has_add_permission(self, request: HttpRequest) -> bool:
        return True

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False
# admin.site.register(MovieApp)
# admin.site.register(Comment)
# admin.site.register(DirectorsList)
# admin.site.register(Reviews)

