from .models import MovieApp, Reviews, Comment


def add_news_tag_category_comment(count):
    for i in range(count):
        reviews = Reviews.objects.create(text=f'Review: {i}')
        movie = MovieApp.objects.create(
            title=f'Movie {i}',
            content=f'comment {i}',
            reviews=reviews
        )
        movie.tags.add(reviews)
        Comment.objects.create(
            movie=movie,
            content=f'Comment {i}',
            author=f'Author {i}'
        )
