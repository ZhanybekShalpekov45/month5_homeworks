from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin
from rest_framework.viewsets import GenericViewSet

from movie_app.models import MovieApp, Comment, DirectorsList, ReviewsList
from movie_app.serializers import DirectorsListSerializer, MovieListSerializer, ReviewsListSerializer, CommentListSerializer
from movie_app.serializers import MovieValidateSerializer, CommentsValidateSerializer, ReviewsValidateSerializer
from rest_framework.generics import ListCreateAPIView, ListAPIView, RetrieveDestroyAPIView, RetrieveUpdateDestroyAPIView


class DirectorListAPIView(ListCreateAPIView):
    queryset = DirectorsList.objects.all()
    serializer_class = DirectorsListSerializer
    pagination_class = None


class MoviesListAPIView(ListCreateAPIView):
    queryset = MovieApp.objects.all()
    serializer_class = MovieListSerializer


class ReviewListAPIView(ListCreateAPIView):
    queryset = ReviewsList.objects.all()
    serializer_class = ReviewsListSerializer
    # pagination_class = PageNumberPagination


class CommentListAPIView(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentListSerializer
    # pagination_class = PageNumberPagination


class DirectorsDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = DirectorsList.objects.all()
    serializer_class = DirectorsListSerializer


class MoviesDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = MovieApp.objects.all()
    serializer_class = MovieListSerializer


class ReviewDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = ReviewsList.objects.all()
    serializer_class = ReviewsListSerializer
    lookup_field = 'id'


class CommentDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentListSerializer
    lookup_field = 'id'


class DirectorViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    queryset = DirectorsList.objects.all()
    serializer_class = DirectorsListSerializer
    lookup_field = 'id'
