from django.contrib import admin
from django.urls import path
from movie_app import views
urlpatterns = [
    path('movie/', views.MoviesListAPIView),
    path('movie/<str:slug>/', views.MoviesDetailAPIView.as_view()),
    path('comments/', views.CommentListAPIView.as_view()),
    path('directors/', views.DirectorListAPIView.as_view()),
    path('directors/<int:director_id>/', views.DirectorsDetailAPIView.as_view()),
    path('movies/<int:comment_id>/', views.CommentDetailAPIView.as_view()),
    path('movies/reviews/', views.ReviewListAPIView.as_view()),
    path('reviews/<int:reviews_id>/', views.ReviewDetailAPIView.as_view()),
    path('movies/directors/<int:director_id>', views.DirectorsDetailAPIView.as_view(
        {'get': 'list', 'post': 'create'}
        )),
]
