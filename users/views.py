import random

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import ConfirmCode
from users.serializers import LoginSerializer, RegisterSerializer, UserSerializer


class LoginAPIView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.data)

        if user:
            token, created_at = Token.objects.get_or_create(user=user)
            return Response({'varification_code': {token.key}})
        return Response({"ERROR": "Couln't send varivication code"}, status=400)


class LogoutAPIView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def delete(self, request):
        request.auth.delete()
        return Response(
            {'message': 'You have been successfully logged out.'},
            status=status.HTTP_200_OK
        )


class RegisterAPIView(CreateAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        code = random.randint(100000, 999999)
        user = User.objects.create_user(is_active=False, **serializer.validated_data)
        ConfirmCode.objects.create(user=user, code=code)

        token, created = Token.objects.get_or_create(user=user)

        return Response({'varification_code': token.key, 'data': serializer.data}, status=201)


class ConfirmAPIView(CreateAPIView):
    def post(self, request, *args, **kwargs):
        code = request.data.get('code')
        if not code:
            return Response({'ERROR': 'NO CODE'}, status=400)

        code = ConfirmCode.objects.filter(code=code).first()

        user = code.user
        user.is_active = True
        user.save()
        code.delete()

        return Response({'message': 'Acccount has been confirmed'}, status=200)


class ProfileAPIView(RetrieveAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user
