from django.urls import path
from users import views


urlpatterns = [
    path('users/login/', views.LoginAPIView.as_view()),
    path('users/register/', views.RegisterAPIView.as_view()),
    path('users/logout/', views.LogoutAPIView.as_view()),
    path('users/profile/', views.ProfileAPIView.as_view()),
    path('users/confirm/', views.ConfirmAPIView.as_view()),
]